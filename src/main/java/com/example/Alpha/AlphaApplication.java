package com.example.Alpha;

import com.example.Alpha.model.User;
import com.example.Alpha.services.KafkaConsumerService;
import com.example.Alpha.services.KafkaProducerService;
import com.example.Alpha.services.UserGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class AlphaApplication {

    @Autowired
    private KafkaProducerService kafkaProducerService;

    @Autowired
    private KafkaConsumerService kafkaConsumerService;

    public static void main(String[] args) {
        SpringApplication.run(AlphaApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void generateAndSendUsers() {
        int count = 0;
        while (count < 1000) {
            User user = UserGenerator.generateUser();
            if (user.getAge() % 2 != 0) {
                kafkaProducerService.sendMessage(user);
            }
            count++;
        }
    }
}
