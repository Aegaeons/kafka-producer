/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Alpha.services;

import com.example.Alpha.model.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 *
 * @author GodSpeed
 */
@Service
public class KafkaConsumerService {

    @KafkaListener(topics = "user_topic", groupId = "user_consumer_group")
    public void consumeMessage(User user) {
        if (user.getAge() % 2 != 0) { 
            System.out.println("Profil Pengguna: " + user.getName() + ", Usia: " + user.getAge());
        }
    }
}
