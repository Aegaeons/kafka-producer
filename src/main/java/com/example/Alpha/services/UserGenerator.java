/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Alpha.services;

import com.example.Alpha.model.User;
import java.util.Random;
import com.github.javafaker.Faker;

/**
 *
 * @author GodSpeed
 */
public class UserGenerator {

    private static final Random random = new Random();

    public static User generateUser() {
        Faker faker = new Faker();
        String name = faker.name().fullName();
        String address = faker.address().fullAddress();
        String phone = faker.phoneNumber().cellPhone();
        int age = random.nextInt(50) + 1; 

        return new User(name, address, phone, age);
    }
}
